## Pachinoscript

Pomomodoro Timer in the terminal, work time, pause time and audio files customizables.

## Dependencies
- [mpg123](https://mpg123.org/) (Optional)
- A notification daemon such as `notification-daemon` or `xfce4-notifyd`

## (Optional) Alarm Sound
First you need to download one or two .mp3 audio files

Open the script and edit the following lines:

``` bash
wtime_audio=" "
ptime_audio=" "
```

Add inside the " " the (absolute) path to the audio file(s).

You can edit the script name if you'd like.

## Usage
The scripts take from zero to three input:
`pachino.sh [OPTIONS] <repetitions> <work_time> <pause_time>`

So, for example if you want to work 50 minutes, pause 10 minutes, two times do:
`pachino.sh 2 50 10`

If you run the script without arguments, it will run the timer once (same as `pachino.sh 1 25 5`).

Add "-s" as optional input to play an alarm sound after the end of each phase

`pachino.sh -s 4 50 10`

